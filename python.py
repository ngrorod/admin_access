from secrets import flag as a
from secrets import seed as g
import re

assert re.match('^[a-zA-Z0-9!.;:,_"#~&°(){}=+*/-]{37}$', a)
assert isinstance(g, int) and g != 25

b=ord;c=map;d=list;e=min;f=g;g=chr;
r="".join(d(c(lambda x,y: g(b(x)+b(y)-2*e(d(c(b,a)))-e(d(c(b,a[f:]+a[:f])))+72),a,a[f:]+a[:f])))
if r == "'W67A^AVNOQ2Rf7D2WKVR[Q7V6(79EaPV`K76":
	print("Admin access granted !")